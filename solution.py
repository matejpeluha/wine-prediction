import pandas
import numpy
import matplotlib.pyplot as plt
import seaborn
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.callbacks import EarlyStopping


def show_average(df, print_text):
    print(print_text + " AVERAGE:")
    print(df.mean())


def show_std(df, print_text):
    print(print_text + " DEVIATION:")
    print(df.std())


def show_hist_column(df, column_text):
    df.hist(column=column_text, bins=100)
    plt.xlabel('Alcohol')
    plt.ylabel('Number of samples')
    plt.show()


def save_normalize_data(df):
    df = df.dropna()
    normalized_df = (df - df.min()) / (df.max() - df.min())
    normalized_df['type'] = normalized_df['type'].apply(numpy.int64)
    normalized_df['quality'] = normalized_df['quality'].apply(numpy.int64)
    normalized_df.to_csv("./csv/wine_test_normalized.csv", index=False)


def start_logistic_regression(df_train, df_test):
    x_train = df_train[['type', 'fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
                        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
    y_train = df_train['quality']
    x_test = df_test[['type', 'fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
                      'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
    y_test = df_test['quality']

    model = LogisticRegression()
    model.fit(x_train, y_train)

    predictions = model.predict(x_test)
    score = model.score(x_test, y_test)
    print(score)

    show_confusion_matrix(y_test, predictions)


def show_confusion_matrix(expected, predictions):
    cm = confusion_matrix(expected, predictions)
    plt.clf()
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Wistia)
    classNames = ['Negative', 'Positive']
    plt.title('Confusion Matrix - Test Data')
    plt.ylabel('Expected(True)')
    plt.xlabel('Predicted')
    tick_marks = numpy.arange(len(classNames))
    plt.xticks(tick_marks, classNames, rotation=45)
    plt.yticks(tick_marks, classNames)
    s = [['TN', 'FP'], ['FN', 'TP']]
    for i in range(2):
        for j in range(2):
            plt.text(j, i, str(s[i][j]) + " = " + str(cm[i][j]))
    plt.show()


def get_model_with_layers(neurons, functions):
    model = Sequential()

    for i in range(len(neurons)):
        model.add(Dense(neurons[i], activation=functions[i]))

    return model


def show_running_results(text, history, val_history):
    plt.plot(history)
    plt.plot(val_history)
    plt.title(text + " history")
    plt.ylabel(text)
    plt.xlabel("epoch")
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()



def start_neural_network(df_train, df_test, layers, layers_functions, loss_function, optimizer, early_stop, epochs, batch_size):
    x_train = df_train[['type', 'fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
                        'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
    y_train = df_train['quality']
    x_test = df_test[['type', 'fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides',
                      'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
    y_test = df_test['quality']

    model = get_model_with_layers(layers, layers_functions)

    model.compile(loss=loss_function, optimizer=optimizer, metrics=['accuracy'])

    if early_stop is None:
        history = model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, verbose=1, validation_data=(x_test, y_test))
    else:
        history = model.fit(x_train, y_train, epochs=epochs, batch_size=batch_size, verbose=1, callbacks=[early_stop],
                            validation_data=(x_test, y_test))

    predictions = model.predict(x_test)
    predictions = numpy.round(predictions).astype(int)
    score = model.evaluate(x_test, y_test, verbose=1)
    print(score)

    show_confusion_matrix(y_test, predictions)
    show_running_results("", history.history['accuracy'], history.history['val_accuracy'])



def start_solution():
    df_train = pandas.read_csv('./csv/wine_train_normalized.csv')
    df_test = pandas.read_csv('./csv/wine_test_normalized.csv')

    early_stop_1 = EarlyStopping(monitor='accuracy', patience=4, min_delta=0.05, mode='max')
    early_stop_2 = EarlyStopping(monitor='accuracy', patience=4, min_delta=0.01, mode='max')
    early_stop_3 = EarlyStopping(monitor='accuracy', patience=4, min_delta=0.005, mode='max')

    # start_neural_network(df_train, df_test, [8, 8, 1], ['relu', 'relu', 'sigmoid'], 'binary_crossentropy', 'sgd', early_stop_1, 15, 1)

    # start_neural_network(df_train, df_test, [8, 16, 1], ['relu', 'relu', 'sigmoid'], 'binary_crossentropy', 'sgd', early_stop_1, 20, 1)

    # start_neural_network(df_train, df_test, [8, 16, 8, 1], ['relu', 'relu', 'relu', 'softmax'], 'binary_crossentropy', 'sgd', early_stop_2, 20, 1)

    # start_neural_network(df_train, df_test, [4, 16, 16, 1], ['exponential', 'exponential', 'exponential', 'sigmoid'], 'categorical_crossentropy', 'adam', early_stop_1, 20, 1)

    # start_neural_network(df_train, df_test, [4, 16, 16, 1], ['exponential', 'exponential', 'exponential', 'sigmoid'], 'binary_crossentropy', 'adam', early_stop_1, 20, 1)

    # start_neural_network(df_train, df_test, [4, 16, 16, 1], ['exponential', 'exponential', 'exponential', 'sigmoid'], 'binary_crossentropy', 'adam', None, 100, 1)

    # start_neural_network(df_train, df_test, [4, 16, 16, 1], ['relu', 'relu', 'relu', 'sigmoid'], 'binary_crossentropy', 'adam', early_stop_3, 30, 1)

    # start_neural_network(df_train, df_test, [12, 24, 1], ['relu', 'relu', 'sigmoid'], 'binary_crossentropy','adam', early_stop_2, 30, 1)

    # start_neural_network(df_train, df_test, [64, 128, 1], ['relu', 'relu', 'sigmoid'], 'binary_crossentropy','sgd', early_stop_2, 30, 1)

   # start_neural_network(df_train, df_test, [24, 16, 1], ['relu', 'relu', 'sigmoid'], 'binary_crossentropy', 'adam', None, 25, 20)

   # start_neural_network(df_train, df_test, [24, 16, 1], ['exponential', 'exponential', 'sigmoid'], 'binary_crossentropy', 'adam', None, 25, 20)

    start_neural_network(df_train, df_test, [12, 24, 1], ['softplus', 'softplus', 'sigmoid'], 'binary_crossentropy', 'adam', None, 50, 20)